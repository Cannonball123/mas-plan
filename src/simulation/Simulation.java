package simulation;

import java.util.concurrent.TimeUnit;

public class Simulation {
	
	public static void main(String[] args) {
		final int WIDTH = 10;
		final int HEIGHT = 10;
		final int AGENTS_NUM = 10;
		final int TIMSTEPS = 20;
		
		
		String board[][] = new String[HEIGHT][WIDTH];
		int tmara[][] = new int[AGENTS_NUM][TIMSTEPS];
		int blocked[] = new int[AGENTS_NUM];
		
		System.out.println("Initiating board");
		initBoard(board);
		printBoard(board);
		
		System.out.println("Initiating tmara");
		initTmara(tmara);
		printTmara(tmara);
		
		System.out.println("Initiating blocked");
		initBlocked(blocked);
		
		System.out.println("Inserting faults");
		block(blocked, 7, 4);
		
		
		System.out.println("Checking tmara");
		
		checkTmara(tmara);
		
		System.out.println("Running simulation");
		
		runSimulation(board, tmara, blocked);
		
	}
	
	static void initBlocked(int[] blocked) {
		for(int i = 0; i < blocked.length; i++) {
			blocked[i] = -1;
		}
	}
	
	static void block(int[] blocked, int agent, int timestep) {
		blocked[agent] = timestep;
	}
	
	static void initBoard(String[][] board) {
		for(int i = 0; i < board.length; i++) {
			for(int j = 0; j < board[0].length; j++) {
				board[i][j] = "_";
			}
		}
		board[0][9] = "0";
		board[0][8] = "1";
		board[0][7] = "2";
		board[0][6] = "3";
		board[0][5] = "4";
		board[9][0] = "5";
		board[9][1] = "6";
		board[9][2] = "7";
		board[9][3] = "8";
		board[9][4] = "9";
	}
	
	static void printBoard(String[][] board) {
		for(int i = 0; i < board.length; i++) {
			for(int j = 0; j < board[0].length; j++) {
				System.out.print(board[i][j] + "\t");
			}
			
			System.out.println("");
		}
	}
	
	static void initTmara(int[][] tmara) {
		// clean up
		for(int i = 0; i < tmara.length; i++) {
			for(int j = 0; j < tmara[0].length; j++) {
				tmara[i][j] = -1;
			}
		}
		// agent 0
		tmara[0][0] = 9;
		tmara[0][1] = 19;
		tmara[0][2] = 29;
		tmara[0][3] = 39;
		tmara[0][4] = 49;
		tmara[0][5] = 48;
		tmara[0][6] = 47;
		tmara[0][7] = 46;
		tmara[0][8] = 45;
		tmara[0][9] = 55;
		tmara[0][10] = 54;
		tmara[0][11] = 53;
		tmara[0][12] = 52;
		tmara[0][13] = 51;
		tmara[0][14] = 50;
		tmara[0][15] = 60;
		tmara[0][16] = 70;
		tmara[0][17] = 80;
		tmara[0][18] = 90;
		tmara[0][19] = -1;
		
		// agent 5
		tmara[5][0] = 90;
		tmara[5][1] = 80;
		tmara[5][2] = 70;
		tmara[5][3] = 60;
		tmara[5][4] = 50;
		tmara[5][5] = 51;
		tmara[5][6] = 52;
		tmara[5][7] = 53;
		tmara[5][8] = 54;
		tmara[5][9] = 44;
		tmara[5][10] = 45;
		tmara[5][11] = 46;
		tmara[5][12] = 47;
		tmara[5][13] = 48;
		tmara[5][14] = 49;
		tmara[5][15] = 39;
		tmara[5][16] = 29;
		tmara[5][17] = 19;
		tmara[5][18] = 9;
		tmara[5][19] = -1;
		
		// agent 1
		tmara[1][0] = 8;
		tmara[1][1] = 18;
		tmara[1][2] = 28;
		tmara[1][3] = 38;
		tmara[1][4] = 37;
		tmara[1][5] = 36;
		tmara[1][6] = 46;
		tmara[1][7] = 56;
		tmara[1][8] = 66;
		tmara[1][9] = 65;
		tmara[1][10] = 64;
		tmara[1][11] = 63;
		tmara[1][12] = 62;
		tmara[1][13] = 61;
		tmara[1][14] = 71;
		tmara[1][15] = 81;
		tmara[1][16] = 91;
		tmara[1][17] = -1;
		
		// agent 6
		tmara[6][0] = 91;
		tmara[6][1] = 81;
		tmara[6][2] = 71;
		tmara[6][3] = 61;
		tmara[6][4] = 62;
		tmara[6][5] = 63;
		tmara[6][6] = 53;
		tmara[6][7] = 43;
		tmara[6][8] = 33;
		tmara[6][9] = 34;
		tmara[6][10] = 35;
		tmara[6][11] = 36;
		tmara[6][12] = 37;
		tmara[6][13] = 38;
		tmara[6][14] = 28;
		tmara[6][15] = 18;
		tmara[6][16] = 8;
		tmara[6][17] = -1;
		
		// agent 2
		tmara[2][0] = 7;
		tmara[2][1] = 17;
		tmara[2][2] = 27;
		tmara[2][3] = 37;
		tmara[2][4] = 47;
		tmara[2][5] = 57;
		tmara[2][6] = 67;
		tmara[2][7] = 77;
		tmara[2][8] = 76;
		tmara[2][9] = 75;
		tmara[2][10] = 74;
		tmara[2][11] = 73;
		tmara[2][12] = 72;
		tmara[2][13] = 82;
		tmara[2][14] = 92;
		tmara[2][15] = -1;
		
		// agent 7
		tmara[7][0] = 92;
		tmara[7][1] = 82;
		tmara[7][2] = 72;
		tmara[7][3] = 62;
		tmara[7][4] = 52;
		tmara[7][5] = 42;
		tmara[7][6] = 32;
		tmara[7][7] = 22;
		tmara[7][8] = 23;
		tmara[7][9] = 24;
		tmara[7][10] = 25;
		tmara[7][11] = 26;
		tmara[7][12] = 27;
		tmara[7][13] = 17;
		tmara[7][14] = 7;
		tmara[7][15] = -1;
		
		// agent 3
		tmara[3][0] = 6;
		tmara[3][1] = 16;
		tmara[3][2] = 26;
		tmara[3][3] = 36;
		tmara[3][4] = 46;
		tmara[3][5] = 56;
		tmara[3][6] = 66;
		tmara[3][7] = 76;
		tmara[3][8] = 86;
		tmara[3][9] = 85;
		tmara[3][10] = 84;
		tmara[3][11] = 83;
		tmara[3][12] = 93;
		tmara[3][13] = -1;
		
		// agent 8
		tmara[8][0] = 93;
		tmara[8][1] = 83;
		tmara[8][2] = 73;
		tmara[8][3] = 63;
		tmara[8][4] = 53;
		tmara[8][5] = 43;
		tmara[8][6] = 33;
		tmara[8][7] = 23;
		tmara[8][8] = 13;
		tmara[8][9] = 14;
		tmara[8][10] = 15;
		tmara[8][11] = 16;
		tmara[8][12] = 6;
		tmara[8][13] = -1;
		
		// agent 4
		tmara[4][0] = 5;
		tmara[4][1] = 15;
		tmara[4][2] = 25;
		tmara[4][3] = 35;
		tmara[4][4] = 45;
		tmara[4][5] = 55;
		tmara[4][6] = 65;
		tmara[4][7] = 75;
		tmara[4][8] = 85;
		tmara[4][9] = 95;
		tmara[4][10] = 94;
		tmara[4][11] = -1;
		
		// agent 9
		tmara[9][0] = 94;
		tmara[9][1] = 84;
		tmara[9][2] = 74;
		tmara[9][3] = 64;
		tmara[9][4] = 54;
		tmara[9][5] = 44;
		tmara[9][6] = 34;
		tmara[9][7] = 24;
		tmara[9][8] = 14;
		tmara[9][9] = 4;
		tmara[9][10] = 5;
		tmara[9][11] = -1;
	}
	
	static void printTmara(int[][] tmara) {
		for(int i = 0; i < tmara.length; i++) {
			for(int j = 0; j < tmara[0].length; j++) {
				System.out.print(tmara[i][j] + "\t");
			}
			
			System.out.println("");
		}
	}
	
	static void checkTmara(int[][] tmara) {
		for(int i = 0; i < tmara[0].length; i++) {
			for(int j = 0; j < tmara.length; j++) {
				for(int a = 0; a != j && a < tmara.length; a++) {
					if(tmara[a][i] != -1 && tmara[j][i] != -1 && tmara[a][i] == tmara[j][i]) {
						System.out.println("tmara has conflict for agents " + a + ", " + j + " at timestamp " + i);
					}
				}
			}
		}
		
		System.out.println("finished checking tmara");
	}
	
	static void runSimulation(String[][] board, int[][] tmara, int[] blocked) {
		int[] newPlaces = new int[tmara.length];
		initNewPlaces(newPlaces, tmara);
		for(int j = 1; j < tmara[0].length; j++) { // loop that goes over the timesteps
			try {
				TimeUnit.MILLISECONDS.sleep(200);;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("step " + j + ":");
			for(int i = 0; i < tmara.length; i++) { // loop that goes over every agent in timestep j
				if(tmara[i][j] != -1 && (blocked[i] == -1 || blocked[i] > j)) { // passing this means agent is not faulty or in post conflict
					if(!board[tmara[i][j]/10][tmara[i][j]%10].equals("_") &&
							blocked[Integer.parseInt(board[tmara[i][j]/10][tmara[i][j]%10])] != -1 &&
							blocked[Integer.parseInt(board[tmara[i][j]/10][tmara[i][j]%10])] <= j) { // passing this means agent is in conflict
						block(blocked, i, j);
					} else { // else means agent is in normal state
						newPlaces[i] = tmara[i][j];
						board[tmara[i][j-1]/10][tmara[i][j-1]%10] = "_";
					}
				} // not entering the top if statement means the agent is faulty or in post conflict
			}
			
			// before we end a timestep we update the board
			for(int i = 0; i < newPlaces.length; i++) {
				board[newPlaces[i]/10][newPlaces[i]%10] = Integer.toString(i);
			}
			
			printBoard(board);
			System.out.println("");
			
		}
		
		System.out.println("end of simulation");
	}
	
	static void initNewPlaces(int[] newPlaces, int[][] tmara) {
		for(int i = 0; i < newPlaces.length; i++) {
			newPlaces[i] = tmara[i][0];
		}
	}
}



